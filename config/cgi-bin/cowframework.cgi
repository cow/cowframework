#!/bin/bash

ENV=`env`
BODY=`/bin/cat /dev/stdin`

PATHINFO=`echo $PATH_INFO`
if [ "$PATHINFO" = "" ]; then
	PATHINFO="/"
fi

/usr/bin/qdbus --system es.ferimer.httpframework $PATHINFO local.DBUSServlet.HTTPQuery "$ENV" "$BODY"
