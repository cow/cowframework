/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file dbusservlet.h
 * @brief DBUSServlet class - This class manage all the DBUS related stuff
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DBUSSERVLET_H
#define DBUSSERVLET_H

#include <QObject>
#include "httprequest.h"
#include "httpresponse.h"

/**
  @class DBUSServlet
  @brief HTTP Servlet DBUS implementation

  This class manages all the DBUS related stuff and receives all the DBUS messages
  */
class DBUSServlet : public QObject
{
    Q_OBJECT

public:
    /**
      @brief Constructor - Creates a new service in the System DBUS in the defined path

      This method creates a new DBUSServlet object listening in DBUS in the define path.

      @param path DBUS path exported in wich the service will be attending
    */
    explicit DBUSServlet(QString path, QObject *parent = 0);

    /// Initialize DBUS. Register the application to the System DBUS
    static void initDBUS(const QString &ServiceName);

signals:
    /**
      @brief Signal emmited when a new connection need to be proccesed.

      The SLOT MUST fill the HTTPResponse object.

      @param[in] request HTTP object that contains the request the client has made
      @param[out] response HTTP object that contains the response to be sent by the framework
    */
    void newconnection(const HTTPRequest &request, HTTPResponse &response);

public slots:
    /**
      @brief SLOT which receives the DBUS notifications

      Processes the Headers and Body and returns the HTTP string to be sent to the client

      @param Headers String with the HTTP RAW headers separated by new lines
      @param Body String with the HTTP RAW body received
      @returns String with the HTTP Response content (Headers and Body)
    */
    QString HTTPQuery(QString Headers, QString Body);

private:
    HTTPRequest m_HTTPRequest;
    HTTPResponse m_HTTPResponse;
};

#endif // DBUSSERVLET_H
