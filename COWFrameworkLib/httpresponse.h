/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file httpresponse.h
 * @brief HTTPResponse class - This class manage all the HTTP Response package
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HTTPRESPONSE_H
#define HTTPRESPONSE_H

#include "httppacket.h"

/**
  @class HTTPResponse
  @brief Class to prepare the HTTP Response packet

  This class has the ability to serialize the headers and body of an HTTPPacket into an HTTP format
  */
class HTTPResponse : public HTTPPacket
{
    Q_OBJECT

public:
    explicit HTTPResponse(HTTPPacket *parent = 0);

public:
    /**
      @brief Geneartes the HTML output based on the headers and the body

      Generates a string with the Headers and Body to be sent to the client

      @returns HTML Packet String
    */
    QString generateHTML(void);

signals:

public slots:

};

#endif // HTTPRESPONSE_H
