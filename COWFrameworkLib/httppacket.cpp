/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file httppacket.cpp
 * @brief HTTPPacket class - This class manage all the HTTP packages stuff
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "httppacket.h"

HTTPPacket::HTTPPacket(QObject *parent) :
    QObject(parent)
{
}

void HTTPPacket::addHeader(const QString name, const QString value) {
    m_Headers.insert(name,value);
}

QString HTTPPacket::getHeader(const QString header) {
    return m_Headers.value(header);
}
