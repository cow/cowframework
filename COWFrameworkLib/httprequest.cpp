/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file httprequest.cpp
 * @brief HTTPRequest class - This class manage all the HTTP Request package
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "httprequest.h"
#include <QStringList>

HTTPRequest::HTTPRequest(HTTPPacket *parent) :
    HTTPPacket(parent)
{
}

void HTTPRequest::setHeaders(const QString headers) {
    m_RAWHeaders = headers;

    QStringList headersList = headers.split(QRegExp("\n"));
    foreach(QString header, headersList) {
        int sep = header.indexOf("=");
        if(sep != -1) {
            addHeader(header.left(sep),header.right(header.size()-sep-1));
        }
    }
}

QString HTTPRequest::getMethod(void) {
    return getHeader("REQUEST_METHOD");
}

void HTTPRequest::parseParameters(void) {
    QString params;

    // We allways proccess the getters
    // GETters
    params = getHeader("QUERY_STRING");
    setParameters(params, m_GET);

    if( getMethod().compare("POST") == 0 ) {
        // POSTers
        params = body();
        setParameters(params, m_POST);
    }
}

void HTTPRequest::setParameters(const QString params, HeadersMap &map) {
    QStringList paramsList = params.split("&");
    foreach(QString param, paramsList) {
        int sep = param.indexOf("=");
        if(sep != -1) {
            map.insert(param.left(sep), param.right(param.size()-sep-1));
        }
    }
}
