# -------------------------------------------------
# Project created by QtCreator 2010-12-01T07:35:04
# -------------------------------------------------
QT += dbus
QT -= gui
TARGET = COWFrameworkLib
TEMPLATE = lib
DEFINES += COWFRAMEWORKLIB_LIBRARY
SOURCES += cowframeworklib.cpp \
    httpresponse.cpp \
    httprequest.cpp \
    dbusservlet.cpp \
    httppacket.cpp
HEADERS += cowframeworklib.h \
    COWFrameworkLib_global.h \
    httpresponse.h \
    httprequest.h \
    dbusservlet.h \
    httppacket.h \
    COWFrameworkLib_global.h
OTHER_FILES += 
