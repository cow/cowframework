/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file dbusservlet.cpp
 * @brief DBUSServlet class - This class manage all the DBUS related stuff
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "dbusservlet.h"
#include <QtDBus/QtDBus>

DBUSServlet::DBUSServlet(QString path, QObject *parent) :
    QObject(parent)
{
    QDBusConnection::systemBus().registerObject(path, this, QDBusConnection::ExportAllSlots);
}

void DBUSServlet::initDBUS(const QString &ServiceName) {
    qDebug("Registering servlet in D-Bus system bus");

    if(!QDBusConnection::systemBus().isConnected()) {
        fprintf(stderr, "Cannot connect to the D-Bus system bus.\n"
                "To start it, run:\n"
                "\teval `dbus-launch --auto-syntax`\n"
                );
        exit(2);
    }

    if(!QDBusConnection::systemBus().registerService(ServiceName)) {
        fprintf(stderr, "%s\n",
                qPrintable(QDBusConnection::systemBus().lastError().message())
                );
        exit(1);
    }
}

QString DBUSServlet::HTTPQuery(QString Headers, QString Body) {
    qDebug(Headers.toAscii().constData());
    qDebug(Body.toAscii().constData());

    m_HTTPRequest.setHeaders(Headers);
    m_HTTPRequest.setBody(Body);

    m_HTTPRequest.parseParameters();

    emit newconnection(m_HTTPRequest, m_HTTPResponse);

    QString m = m_HTTPRequest.getMethod();
    if( m.compare("GET") == 0 )
        qDebug("GET");
    else if( m.compare("PUT") == 0 )
        qDebug("PUT");
    else if( m.compare("POST") == 0 )
        qDebug("POST");
    else if( m.compare("DELETE") == 0 )
        qDebug("DELETE");
    else
        qDebug("ERROR");

    return m_HTTPResponse.generateHTML();
}
