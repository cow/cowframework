/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file httppacket.h
 * @brief HTTPPacket class - This class manage all the HTTP packages stuff
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HTTPPACKET_H
#define HTTPPACKET_H

#include <QObject>
#include <QMap>

/// Map to store all the HTTP Headers key-values
typedef QMap<QString,QString> HeadersMap;

/**
  @class HTTPPacket
  @brief Base class which manages the headers and body of an HTTP packet

  This class manages the headers and body of an HTTP packet
  */
class HTTPPacket : public QObject
{
    Q_OBJECT
    Q_PROPERTY(HeadersMap m_Headers READ headers WRITE setHeaders)
    Q_PROPERTY(QString m_Body READ body WRITE setBody)

public:
    explicit HTTPPacket(QObject *parent = 0);

    /**
      @brief Adds a new Key-Value keader to the headers map

      Add the header value to the headers map

      @param name Header name
      @param value Header value
    */
    void addHeader(const QString name, const QString value);

// Properties
public:
    void setHeaders(HeadersMap headers) { m_Headers = headers; }
    inline HeadersMap headers() const { return m_Headers; }
    void setBody(QString body) { m_Body = body; }
    inline QString body() const { return m_Body; }

    QString getHeader(const QString header);

signals:

public slots:

protected:
    HeadersMap m_Headers;
    QString m_Body;

};

#endif // HTTPPACKET_H
