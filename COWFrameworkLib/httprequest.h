/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file httprequest.h
 * @brief HTTPRequest class - This class manage all the HTTP Request package
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include "httppacket.h"

/**
  @class HTTPRequest
  @brief This class proccess the HTTP Requests received

  This class parse the HTTP Request to obtain the headers and the body (deserialize)
  */
class HTTPRequest : public HTTPPacket
{
    Q_OBJECT
    Q_PROPERTY(QString m_RAWHeaders READ rawheaders)

public:
    explicit HTTPRequest(HTTPPacket *parent = 0);
    void parseParameters(void);

private:
    void setParameters(const QString params, HeadersMap &map);

// Properties
public:
    inline QString rawheaders() const { return m_RAWHeaders; }

    /**
      @brief Parses RAW headers string into the headers map

      Parses a string with the headers (separated by new lines) and stores it in the headers map.

      @param headers String with the headers
    */
    void setHeaders(const QString headers);

    /** This is for convenience */
    QString getMethod(void);
    inline HeadersMap GETParameters() const { return m_GET; }
    inline HeadersMap POSTParameters() const { return m_POST; }
    inline HeadersMap Parameters() const { HeadersMap aux; aux.unite(m_GET); aux.unite(m_POST); return aux; }

signals:

public slots:

private:
    QString m_RAWHeaders;
    HeadersMap m_GET;
    HeadersMap m_POST;
};

#endif // HTTPREQUEST_H
