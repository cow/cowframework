/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file cowframeworklib.h
 * @brief COWFrameworkLib class - This class exports methods to use by the client software
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COWFRAMEWORKLIB_H
#define COWFRAMEWORKLIB_H

#include "COWFrameworkLib_global.h"
#include "dbusservlet.h"

/**
  @class COWFrameworkLib
  @brief Interface class which exports the methods to be used by the client programs

  This class acts as an interface to exports the methods to be used by the client programs
  */
class COWFRAMEWORKLIBSHARED_EXPORT COWFrameworkLib {
public:
    COWFrameworkLib(const char *serviceName);

    /**
      @brief Create a new servlet

      Creates a new servlet listening in DBUS in the defined path and each connection will be signalled to the method SLOT in the receiver object.

      @param path DBUS Path to attend petitions
      @param receiver Object with the SLOT which will process the HTTPRequest and fill the HTTPResponse object
      @param method Slot which will process the HTTPRequest and fill the HTTPResponse object
      @returns The new DBUSServlet generated object. The memory management of the returned pointer is responsability of the caller.
    */
    DBUSServlet *createServlet(const QString path, const QObject *reciever = 0, const char* method = 0);
};

#endif // COWFRAMEWORKLIB_H
