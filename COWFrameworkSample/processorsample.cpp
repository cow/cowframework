/**
 * COW Framework - A C++ Oriented Web Framework
 *
 * @file processorsample.cpp
 * @brief ProcessorSample - This class implements the SLOTs which process the HTTP connections
 *
 * @author Fernando Rodríguez Sela <frsela@gmail.com>
 * @date December 2010
 * @version 0.1
 *
 * @copyright (c) 2010 Fernando Rodríguez Sela <frsela@gmail.com>
 * @license GNU/LGPL v3.0, see COPYING and COPYING.LESSER
 *
 *  This file is part of COWFramework.
 *
 *  QtWebFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QtWebFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QtWebFramework.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "processorsample.h"

ProcessorSample::ProcessorSample(QObject *parent) :
    QObject(parent)
{
}

void ProcessorSample::slot1(const HTTPRequest &request, HTTPResponse &response) {
    QString body;
    body = "<h1>COW Framework - C++ Oriented Web echo sample</h1>";

    body += "<h2>RAW Headers</h2><pre>";
    body += request.rawheaders();

    body += "</pre><h2>Headers</h2><ul>";
    HeadersMap h = request.headers();
    foreach (QString key, h.keys()) {
        body += "<li>";
        body += key;
        body += ": ";
        body += h.value(key);
        body += "</li>";
    }
    body += "</ul><br />";

    body += "<h2>Body</h2><pre>";
    body += request.body();
    body += "</pre>";

    body += "<h2>GET Parameters</h2><ul>";
    h = request.GETParameters();
    foreach (QString key, h.keys()) {
        body += "<li>";
        body += key;
        body += ": ";
        body += h.value(key);
        body += "</li>";
    }
    body += "</ul><br />";


    body += "<h2>POST Parameters</h2><ul>";
    h = request.POSTParameters();
    foreach (QString key, h.keys()) {
        body += "<li>";
        body += key;
        body += ": ";
        body += h.value(key);
        body += "</li>";
    }
    body += "</ul><br />";

    body += "<h2>ALL Parameters</h2><ul>";
    h = request.Parameters();
    foreach (QString key, h.keys()) {
        body += "<li>";
        body += key;
        body += ": ";
        body += h.value(key);
        body += "</li>";
    }
    body += "</ul><br />";

    response.addHeader("Content-Type","text/html");
    response.setBody(body);

    response.addHeader("Set-Cookie","prueba=galletita");
}
