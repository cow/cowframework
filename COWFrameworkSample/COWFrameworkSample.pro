# -------------------------------------------------
# Project created by QtCreator 2010-12-01T07:39:41
# -------------------------------------------------
QT -= gui
TARGET = COWFrameworkSample
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += main.cpp \
    processorsample.cpp
LIBS += -L../COWFrameworkLib \
    -lCOWFrameworkLib
HEADERS += processorsample.h
